WHAT IT IS
----------
This Python script (realchemy.py) is for finding possible Alchemy recipes in Skyrim with Requiem overhaul. It comes with SQLite3 database ingdata.db, which stores information about ingredients, their effects, and effect groups.
Python used for development is 3.4.3, but there should be no reason for it not to work with 2.7 version.
To download python use: https://www.python.org/downloads/

USAGE
-----
This is command line only tool, it takes specific arguments and prints out results to console (and file if using -o option).

	usage: realchemy.py [-h] [-o OUTPUT_FILE] {player,list,recipe} ...
	
	-h : help, for ex. 'realchemy recipe -h' for recipe command help

	-o OUTPUT_FILE - Also output to a specified file, make sure you are not overwriting any useful file.
	
	player - Main command for setting player statistics used for calculations, use no flags if you want to check what settings are set right now
	
	
    -a ALCHEMY_LEVEL, --alchemy-level ALCHEMY_LEVEL
                        Set player Alchemy level
    -f FORTIFY_ALCHEMY, --fortify-alchemy FORTIFY_ALCHEMY
                        Set player Alchemy fortification level
    -l {0,1,2}, --lore-level {0,1,2}
                        Set player Alchemical Lore level
    -e {Y,y,N,n}, --improved-elixirs {Y,y,N,n}
                        Set player Improved Elixirs Perk
    -p {Y,y,N,n}, --improved-poisons {Y,y,N,n}
                        Set player Improved Poisons Perk
    -r {Y,y,N,n}, --purification {Y,y,N,n}
                        Set player Purification Perk
	
	list - Main command for displaying ingredient, effect, or effect groups if you want to find out what you can use with 'recipe' command
	
	
	'list' command has these flags:
	-i [INGREDIENT [INGREDIENT ...]], --ingr-info [INGREDIENT [INGREDIENT ...]]
                        Display ingredient info, do not use any INGREDIENT for all ingredients
	-e [EFFECT [EFFECT ...]], --effect-info [EFFECT [EFFECT ...]]
                        Display effect info, do not use any EFFECT for all effects
	-g [GROUP [GROUP ...]], --group-info [GROUP [GROUP ...]]
                        Display group info, do not use any GROUP for all groups
	
	USAGE EXAMPLES:
	'realchemy.py list -i'  lists all ingredients
	'realchemy.py list -g Effect'  list all effect groups with "Effect" in their name
	
	
	recipe - Main command for finding matching recipes, has flags for filtering
	
	'recipe' command has these flags:
	-n N, --min-effects N
                        Only potions with more or equal effects than minimum
	-m M, --max-effects M
                        Only potions with less or equal effects than maximum
	-i INGREDIENT [INGREDIENT ...], --must-have-ingredient INGREDIENT [INGREDIENT ...]
                        Only potions which include selected ingredients
	-I INGREDIENT [INGREDIENT ...], --must-not-have-ingredient INGREDIENT [INGREDIENT ...]
                        Only potions which exclude selected ingredients
	-g GROUP [GROUP ...], --must-be-group GROUP [GROUP ...]
                        Only potions with all effects in selected groups
	-G GROUP [GROUP ...], --must-not-be-group GROUP [GROUP ...]
                        Only not potions with any effect in selected groups
	-e EFFECT [EFFECT ...], --must-have-effect EFFECT [EFFECT ...]
                        Only potions with all selected effects
	-E EFFECT [EFFECT ...], --must-not-have-effect EFFECT [EFFECT ...]
                        Only not potions with any of selected effects
	-p, --only-positives  Potions must have all positive effects
	-P, --only-poisons    Potions must have all poisonous effects
	USAGE EXAMPLES:
	'realchemy.py recipe -n 4 -P -e Affliction -i "Nirnroot (S)"' - list potion with minimum of four effects, must be pure poisons, must have Affliction effect and one of the ingredients must be Nirnroot (Standard, non Crimson)
	
	As it is right now, recipes are printed out ordered by price (biggest first).
	I will consider adding sorting options in later releases.
	
	
Use list command to find out what ingredients, effects, and groups are available.  
Use recipe command to find specific recipes.  
	
Project is hosted on https://gitlab.com/QuasiMod/re-alchemy