__author__ = 'Ugnius'
import sys
import sqlite3
import argparse
import itertools
import collections
import math

original = sys.stdout

# classes

class Consts(object):
    def __init__(self):
        self.restoreHealthId = list(getEffectIds("Healing"))[0]
        self.restoreMagickaId = list(getEffectIds("Restore Magicka"))[0]
        self.restoreStaminaId = list(getEffectIds("Restore Stamina"))[0]
        self.ingredientMult = 4.0
        self.alchemySkillFactor = 1.1

class Player(object):
    def __init__(self, alchemySkill = 0, fortifyAlchemy = 0, alchemicalLoreLavel = 0, hasPerkImprovedElixirs = False, hasPerkImprovedPoisons = False, hasPerkPurification = False):
        self.alchemySkill = alchemySkill
        self.fortifyAlchemy = fortifyAlchemy
        self.hasPerkHerbalism = False;
        self.alchemicalLoreLevel = alchemicalLoreLavel # Alchemist perk
        self.hasPerkImprovedElixirs = hasPerkImprovedElixirs # Benefactor
        self.hasPerkImprovedPoisons = hasPerkImprovedPoisons # Poisoner Perk
        self.hasPerkPurification = hasPerkPurification # Purification perk

    def print_self(self):
        print('Player Alchemy is Level {0}, Fortified By {1}, Alchemical Lore perk is Level {2}, Improved Elixirs perk: {3}, Improved Poisons perk: {4}, Purification perk: {5}'.format(self.alchemySkill, self.fortifyAlchemy, self.alchemicalLoreLevel, "Yes" if self.hasPerkImprovedElixirs else "No", "Yes" if self.hasPerkImprovedPoisons else "No", "Yes" if self.hasPerkPurification else "No"))

class Ingredient(object):
    id = None
    name = ""
    value = ""
    effects = []
    def __init__(self, id, name, value, effects=[]):
        self.id = id
        self.name = name
        self.value = value
        self.effects = effects
        for eff in effects:
            eff.ingredient = self

    def printself(self, short = False):
        if short:
            print('{0} ({1})'.format(self.name, ', '.join(eff.reName for eff in self.effects)))
        else:
            print('ID: {0}, Name: {1}, Value: {2}'.format(self.id, self.name, self.value))
            if len(self.effects) > 0:
                for eff in self.effects:
                    eff.printself()

class EffectFlags:
    no_magnitude = False
    no_duration = False
    power_affects_magnitude = False
    power_affects_duration = False
    hostile = False
    recover = False
    detrimental = False
    no_area = False
    fx_persist = False
    dispel_with_keywords = False
    def __init__(self, sFlags):
        if sFlags is None or sFlags == "":
            return
        for flag in sFlags.lower().split(', '):
            if flag == "no magnitude":
                self.no_magnitude = True
            elif flag == "no duration":
                self.no_duration = True
            elif flag == "power affects magnitude":
                self.power_affects_magnitude = True
            elif flag == "power affects duration":
                self.power_affects_duration = True
            elif flag == "hostile":
                self.hostile = True
            elif flag == "recover":
                self.recover = True
            elif flag == "detrimental":
                self.detrimental = True
            elif flag == "no area":
                self.no_area = True
            elif flag == "fx persist":
                self.fx_persist = True
            elif flag == "dispel with keywords":
                self.dispel_with_keywords = True
            else:
                print('No flag "{0}"'.format(flag))

EffectPair = collections.namedtuple('EffectPair', ['first', 'second'])

class Effect(object):
    id = None
    ingrId = None
    ingredient = None
    nameId = None
    reName = ""
    effName = ""
    groupId = None
    groupName = ""
    basecost = 1.0
    magnitude = 0.0
    duration = 0.0
    description = ""
    priority = 100
    def __init__(self, id, ingrId, nameId, reName, effName, groupId,  groupName, basecost, magnitude, duration, description, priority, magadj, duradj, costadj, sflags):
        self.id = id
        self.ingrId = ingrId
        self.nameId = nameId
        self.reName = reName
        self.effName = effName
        self.groupId = groupId
        self.groupName = groupName
        self.basecost = basecost
        self.magnitude = magnitude
        self.duration = duration
        self.description = description
        self.priority = priority
        self.magadj = magadj
        self.duradj = duradj
        self.costadj = costadj
        self.flags = EffectFlags(sflags)
        self.preCost()

    # Functions for determining effect power
    def AlchemistPerk(self):
        # Those values fit better in Requiem
        if player.alchemicalLoreLevel > 1:
            return 50
        elif player.alchemicalLoreLevel > 0:
            return 25
        else:
            return 0

    def PhysicianPerk(self):
        # if player.hasPerkHerbalism and self.nameId in {consts.restoreHealthId, consts.restoreMagickaId, consts.restoreStaminaId}:
        if player.hasPerkImprovedElixirs and self.nameId in {consts.restoreHealthId, consts.restoreMagickaId, consts.restoreStaminaId}:
            return 25
        else:
            return 0

    def BenefactorPerk(self, making_poison = False):
        if player.hasPerkImprovedElixirs and not making_poison and self.isBeneficial():
            return 25
        else:
            return 0

    def PoisonerPerk(self, making_poison = True):
        if player.hasPerkImprovedPoisons and making_poison and not self.isBeneficial():
            return 25
        else:
            return 0

    def PurificationPerk(self, making_poison = True):
        if player.hasPerkPurification:
            # Perk instantly increases potion power by 80%
            if making_poison and not self.isBeneficial():
                return 80 # 180%
            elif self.nameId in {consts.restoreHealthId, consts.restoreMagickaId, consts.restoreStaminaId}:
                return 170 # 150% * 180% = 270% - Restore spels by another 50%
            else:
                return 80 # 180%
        else:
            return 0

    # Power factor function, as defined in UESP Wiki
    def PowerFactor(self, making_poison=False):
        alchemistPerk = self.AlchemistPerk()
        physicianPerk = self.PhysicianPerk()
        benefactorPerk = self.BenefactorPerk(making_poison)
        poisonerPerk = self.PoisonerPerk(making_poison)
        purificationPerk = self.PurificationPerk(making_poison)
        return consts.ingredientMult * (1 + (consts.alchemySkillFactor - 1) * player.alchemySkill / 100) * (1 + player.fortifyAlchemy / 100) * (1 + alchemistPerk / 100) * (1 + physicianPerk / 100) * (1 + benefactorPerk / 100 + poisonerPerk / 100) * (1 + purificationPerk / 100)

    def preCost(self):
        self.ppower, self.pduration, self.pval = self.getMagnitude(not self.isBeneficial())

    # Main magnitude, duration and price, calculation function
    def getMagnitude(self, making_poison=False):

        # For some potions need to decrease strength by 50%, not sure why, needs testing
        mag_adjust = 0.5 if self.groupId in {3} else 1.0

        if player.alchemicalLoreLevel > 0:
            powerFactor = self.PowerFactor(making_poison) * mag_adjust
        else:
            powerFactor = 0.0

        magnitude = self.magnitude

        if self.flags.no_magnitude:
            magnitude = 0
        magnitudeFactor = 1.0
        if self.flags.power_affects_magnitude:
            magnitudeFactor = powerFactor

        magnitude = round(magnitude * magnitudeFactor)

        duration = self.duration
        if self.flags.no_duration:
            duration = 0
        durationFactor = 1.0
        if self.flags.power_affects_duration:
            durationFactor = powerFactor
        duration = round(duration * durationFactor)

        magnitudeFactor = 1.0
        if (magnitude > 0 ):
            magnitudeFactor = magnitude
        durationFactor = 1.0
        if (duration > 0 ):
            durationFactor = duration / 10

        # Cost currently does not calculate properly, Cannot find what's wrong right now
        value = math.floor(self.basecost * math.pow(magnitudeFactor * durationFactor, 1.1))

        '''
        # Alternative cost calculation
        Magnitude = self.magnitude * consts.ingredientMult * math.pow(consts.alchemySkillFactor, player.alchemySkill / 100) * (1 + player.fortifyAlchemy / 100) * (1 + self.AlchemistPerk() / 100) * (1 + self.PhysicianPerk() / 100) * (1 + self.BenefactorPerk(making_poison) / 100 + self.PoisonerPerk(making_poison) / 100)
        Duration = self.duration
        value = math.floor(self.basecost * math.pow(Magnitude, 1.1) * 0.0794328 * math.pow(Duration, 1.1))
        '''

        return magnitude, duration, value

    def getEffectString(self, magnitude, duration):
        # magnitude, duration, value = self.getMagnitude(making_poison)
        if self.description is not None:
            return self.description.format(mag = magnitude, dur = duration)
        else:
            return "{name} of {mag} for {dur}".format(name = self.reName, mag = magnitude, dur = duration)

    def getSelfEffectString(self):
        if hasattr(self, 'mag') and hasattr(self, 'dur'):
            return self.getEffectString(self.mag, self.dur)
        else:
            return None

    def isBeneficial(self):
        return self.groupId in {1, 2, 3, 4}

    def printself(self):
        print("ID: {0}, Ingredient ID: {1}, Ingredient Name: {2}, Name: {3}, Effect: {4}, Group: {5}, Base Cost: {6}, Magnitude: {7}, Duration: {8}, Priority: {9}".format(self.id, self.ingrId, self.ingredient.name, self.reName, self.effName, self.groupName, self.basecost, self.magnitude, self.duration, self.priority))

class Recipe(object):
    ingredients = []
    effects = []
    def __init__(self, ingredients, effects):
        self.ingredients = ingredients
        self.effects = sorted(effects, key=lambda eff: eff.first.pval, reverse=True)
        if player.hasPerkPurification:
            if (self.isPoison()):
                self.effects = list(filter(lambda eff: not eff.first.isBeneficial(), self.effects))
            else:
                self.effects = list(filter(lambda eff: eff.first.isBeneficial(), self.effects))
        self.totalv = 0;
        for eff in self.effects:
            eff.first.mag, eff.first.dur, eff.first.val = self.getAdjEffectValues(eff)
            self.totalv += eff.first.val
        # self.totalv = math.floor(self.totalv)

    def isPoison(self):
        # Test if effect with biggest value is poison
        return not self.effects[0].first.isBeneficial()

    def effNameIds(self):
        return {eff.first.nameId for eff in self.effects}

    def containsAnyEffect(self, *effNameIdList):
        effNameIds = self.effNameIds()
        return any((effNameId in effNameIds) for effNameId in effNameIdList)

    def containsAllEffects(self, *effNameIdList):
        # Not used yet
        effNameIds = self.effNameIds()
        return all((effNameId in effNameIds) for effNameId in effNameIdList)

    # Get Adjusted values for effect
    # ToDo: This function does nothing right now, needs testing if adjustments are needed in any way
    def getAdjEffectValues(self, eff):
        # mag, dur, val = eff[0].getMagnitude(self.isPoison())
        # return round(mag * eff[0].magadj) , round(dur * eff[0].duradj), round(val * eff[0].costadj)
        return eff.first.getMagnitude(self.isPoison()) # Do Not do nothing to values

    def printself(self, short = False):
        ingrnames = ' + '.join([ingr.name for ingr in self.ingredients])
        # Print effects
        if short:
            if self.isPoison():
                potionname = "Poison of {0}".format(self.effects[0].first.reName)
            else:
                potionname = "Potion of {0}".format(self.effects[0].first.reName)

            effdesc = []
            totalv = 0
            for eff in self.effects:
                # this op duplicates from constructor, only for debugging specific cases
                mag, dur, val = eff.first.getMagnitude(self.isPoison())
                totalv += val
                effdesc.append(eff.first.getEffectString(mag, dur))

            # self.totalv = round(self.totalv)

            enamelist = [eff.first.reName for eff in self.effects]
#            for eff in self.effects:
#                print(eff.reName, getMagnitude(eff))
            positives = [eff.first.groupId in set((1, 2, 3, 4)) for eff in self.effects]
            negatives = [eff.first.groupId in set((5, 6, 7, 8)) for eff in self.effects]
            effects = [str(('+' if v[0] else '-') + v[1]) for v in zip(positives, enamelist)]
            effnames = ', '.join(effects)
            effdescstr = '\n   '.join(effdesc)
            print('{0} = {4} ({1} [+{2}, -{3}] {5}), Value: {6}\n   {7}'.format(ingrnames, len(enamelist), sum(positives), sum(negatives), potionname, effnames, totalv, effdescstr))
            # print(positives, ' -- ', negatives)
        else:
            print(ingrnames)
            for eff in self.effects:
                eff.first.printself()

# output redirector class
class Tee(object):
    def __init__(self, *files):
        self.files = files
    def write(self, obj):
        for f in self.files:
            f.write(obj)
    def flush(self):
        for f in self.files:
            f.flush()

# database query functions
def getIngredientEffects(forIngrID):
    effects = []
    cursor.execute('''SELECT IngrEffects.ID as EffectID, IngrID, EffNameID, ReqName, EffectName, EffectGroup as EffectGroupID, GroupName as EffectGroupName, BaseCost, Magnitude, Duration, EffectDescription, Priority, MagAdjust, DurAdjust, CostAdjust, Flags
                        FROM IngrEffects
                        JOIN EffectNames ON IngrEffects.EffNameID = EffectNames.ID
                        JOIN EffectGroups ON EffectNames.EffectGroup = EffectGroups.ID
                        WHERE IngrID = ?''', (forIngrID,))
    for erow in cursor:
        effect = Effect(erow[0], erow[1], erow[2], erow[3], erow[4], erow[5], erow[6], erow[7], erow[8], erow[9], erow[10], erow[11], erow[12], erow[13], erow[14], erow[15])
        effects.append(effect)
    return effects

def getEffectsByName(effName):
    if effName == '*':
        cursor.execute('''SELECT EffectNames.ID as ID, ReqName, EffectName, EffectGroup, GroupName FROM EffectNames
                            JOIN EffectGroups ON EffectNames.EffectGroup = EffectGroups.ID''')
    else:
        cursor.execute('''SELECT EffectNames.ID as ID, ReqName, EffectName, EffectGroup, GroupName FROM EffectNames
                            JOIN EffectGroups ON EffectNames.EffectGroup = EffectGroups.ID
                            WHERE ReqName LIKE ?''', ('%'+effName+'%',))
    return cursor.fetchall()


def getEffectIds(effect_name):
    # cursor.execute('''SELECT ID, ReqName FROM EffectNames WHERE ReqName LIKE ?''', ('%'+effect_name+'%',))
    effects = getEffectsByName(effect_name)
    return set((row[0] for row in effects))

def getIngredients(ingrname):
    if ingrname == '*':
        cursor.execute('''SELECT ID, Name, Value FROM Ingredients''')
    else:
        cursor.execute('''SELECT ID, Name, Value FROM Ingredients WHERE Name LIKE ?''', ('%'+ingrname+'%',))
    result = []
    rows = cursor.fetchall()
    for row in rows:
        # find effects for ingredient
        effects = getIngredientEffects(row[0])
        item = Ingredient(row[0], row[1], row[2], effects = effects)
        result.append(item)
    return result

def getIngredientIds(ingr_name):
    cursor.execute('''SELECT ID FROM Ingredients WHERE Name LIKE ?''', ('%'+ingr_name+'%',))
    return set((row[0] for row in cursor.fetchall()))

# Get group info by name
def getGroupsByName(groupname):
    if groupname == '*':
        cursor.execute('''SELECT ID, GroupName FROM EffectGroups''')
    else:
        cursor.execute('''SELECT ID, GroupName FROM EffectGroups WHERE GroupName LIKE ?''', ('%'+groupname+'%',))

    return cursor.fetchall()

# Get group ID's by name
def getGroupIds(groupname):
    rows = getGroupsByName(groupname)
    return set((row[0] for row in rows))

# create recipe from ingredient, returns None if no recipe or has superflous ingredients
def mixRecipe(ingredients):

    # get best, and second best effects - two ingredient effects used in making potion effect
    def getBestEffect(effid):
        first = None
        second = None
        # Find ingredient used for effect, right now Value of an effect is a factor
        # ToDo: I don't know how this is done in Skyrim, some effects are selected differently, Like "Blisterwort" and "Imp Stool"
        # This is a bug
        for ing in ingredients:
            for eff in ing.effects:
                if eff.nameId == effid:
                    if first is None or eff.pval > first.pval:
                        first, second = eff, first
                    elif second is None or eff.pval > second.pval:
                        second = eff
        return EffectPair(first=first, second=second)

    # get all ingredient effect ID sets, and combine them in pairs
    effcombs = itertools.combinations(({eff.nameId for eff in ing.effects} for ing in ingredients), 2)
    # get all effect ID's matching in all effect pairs
    all_effids = set.union(*(set.intersection(effc[0], effc[1]) for effc in effcombs))

    # get set of effect objects found when combining ingredients,
    # also best and second best effect object used to make up effect,
    # and set of ingredient ID's used to make all effects
    # ToDo: This part si still buggy
    alleff = set()
    ingrused = set()
    for eff in all_effids:
        ep = getBestEffect(eff)
        alleff.add(ep)
        ingrused = ingrused.union({ep.first.ingrId, ep.second.ingrId})

    if len(ingrused) < len(ingredients):
        return None # If superflous ingredients (potions with no effects included) - drop recipe
    else:
        return Recipe(ingredients, alleff)

# print all recipes in recipes list, short means single line mode
def printRecipes(recipes, short = True):
    for recipe in recipes:
        recipe.printself(short)

# get recipes combining all ingrlist ingredients and filtering them through filters
def make_PercentBar(total, len, stdout=sys.stdout, label=""):
    point = total / 100
    increment = total / len
    def pctBar(i):
        if(i % (5 * point) == 0):
            stdout.write("\r"+label+"[" + "=" * int(i / increment) +  " " * int((total - i)/ increment) + "]" +  str(int(i / point)) + "%")
            stdout.flush()
    return pctBar

def getRecipes(ingrlist, filters = None):
    rlist = []

    # Sub function, adds arecipe to rlist if valid recipe and passes all the filters (if any)
    def add_checked_recipe(arecipe):
        if arecipe is not None:
            if filters is None or all(filter(arecipe) for filter in filters):
                rlist.append(arecipe)

    # Generate recipes from combinations of 2 ingredients
    l2 = len(ingrlist)
    c2 = int(l2 * (l2 - 1) / 2)
    l3 = len(ingrlist)
    c3 = int(l3 * (l3 - 1) * (l3 - 2) / 6)
    i = 0;
    pct_bar = make_PercentBar(c2 + c3, 40, original, label="Calculating ");
    for comb in itertools.combinations(ingrlist, 2):
        add_checked_recipe(mixRecipe(comb))
        i+=1
        pct_bar(i)

    # Generate recipes from combinations of 3 ingredients
    for comb in itertools.combinations(ingrlist, 3):
        add_checked_recipe(mixRecipe(comb))
        i+=1
        pct_bar(i)
    original.write('\n')
    return rlist

# filter factories:
def make_minfilter(min_effects=1):
    def rfilter(recipe):
        return len(recipe.effects) >= min_effects

    return rfilter

def make_maxfilter(max_effects=1):
    def rfilter(recipe):
        return len(recipe.effects) <= max_effects

    return rfilter

def make_ingrfilter(ingr_ids=set(), and_have = False, not_have = False):
    def rfilter(recipe):
        ringrids = set(ingr.id for ingr in recipe.ingredients)

        if and_have:
            return (ingr_ids <= ringrids) != not_have
        else:
            return (len(ingr_ids & ringrids) > 0) != not_have

    return rfilter

def make_groupfilter(groups=set(), and_have = False, not_have = False):
    def rfilter(recipe):
        rgroupids = set(eff[0].groupId for eff in recipe.effects)

        if and_have:
            return (groups >= rgroupids) != not_have
        else:
            return (len(groups & rgroupids) > 0) != not_have

    return rfilter

def make_effectfilter(effects=set(), and_have = False, not_have = False):
    def rfilter(recipe):

        reffectids = set(eff[0].nameId for eff in recipe.effects)

        if and_have:
            return (effects <= reffectids) != not_have
        else:
            return (len(effects & reffectids) > 0) != not_have

    return rfilter

# parameter functions
# list ingredient
def list_ingredients(args):
    # ingredients
    if (args.ingr_info is None):
        # allingr = getAllIngredients()
        pass
    else:
        allingr = []
        if (len(args.ingr_info) > 0):
            for ingrname in args.ingr_info:
                ingrlist = getIngredients(ingrname)
                allingr.extend(ingrlist)
                if len(ingrlist) == 0:
                    print('No match for ingredient "{0}"'.format(ingrname))
        else:
            allingr = getIngredients('*')

        print('Ingredients listed: {0}'.format(len(allingr)))
        for ingr in allingr:
            ingr.printself(short = True)

    # effects
    if(args.effect_info is not None):
        if (len(args.effect_info) > 0):
            effects = []
            for effName in args.effect_info:
                eff = getEffectsByName(effName)
                effects.extend(eff)
                if len(eff) == 0:
                    print('No match for effect "{0}"'.format(effName))
        else:
            effects = getEffectsByName('*')
        # Print Effects
        for eff in effects:
            print('{0}{1} ({2}), Group: {3}'.format('+' if eff[3] in (1, 2, 3, 4) else '-', eff[1], eff[2], eff[4]))

    # group
    if(args.group_info is not None):
        if (len(args.group_info) > 0):
            groups = []
            for groupName in args.group_info:
                grp = getGroupsByName(groupName)
                groups.extend(grp)
                if len(grp) == 0:
                    print('No match for group "{0}"'.format(groupName))
        else:
            groups = getGroupsByName('*')
        # Print Effects
        for group in groups:
            print('{0}{1}'.format('+' if group[0] in (1, 2, 3, 4) else '-', group[1]))

# list recipes
def list_recipes(args):
    global player
    player = fetchPlayerStats()
    player.print_self()
    print('You can use \'player\' command to change those values.')

    # print(args)
    filters = []
    allingr = getIngredients('*')

    if args.min_effects is not None:
        filters.append(make_minfilter(min_effects=args.min_effects[0]))

    if args.max_effects is not None:
        filters.append(make_maxfilter(max_effects=args.max_effects[0]))

    if args.min_effects is not None and args.max_effects is not None and args.min_effects[0] > args.max_effects[0]:
        print("WARNING: minimum effect count value is bigger than maximum effect count value")

    # To-Do: Ingredients, groups, effects might be ambiguous and filter may be to strict
    # possibly print what filters, with what options are on, with warning on too strict filter (> 3 ingrediants)
    all_ingr_ids_in = set()
    if args.must_have_ingredient is not None:
        for ingr_name in args.must_have_ingredient:
            curr_ingr_ids = getIngredientIds(ingr_name)
            if len(curr_ingr_ids) > 1:
                print('INFO: Ingredient inclusion filter "{0}" returns ambiguous results ({1})'.format(ingr_name, len(curr_ingr_ids)))
            elif len(curr_ingr_ids) == 0:
                print('INFO: Ingredient inclusion filter "{0}" returned no results, check using "list -i" command'.format(ingr_name))
            all_ingr_ids_in |= curr_ingr_ids

        if len(all_ingr_ids_in) > 3:
            print("WARNING: Ingredient filter too strict, it contains more than 3 must have ingredients")
        filters.append(make_ingrfilter(all_ingr_ids_in, and_have=True))

    all_ingr_ids_out = set()
    if args.must_not_have_ingredient is not None:
        for ingr_name in args.must_not_have_ingredient:
            curr_ingr_ids = getIngredientIds(ingr_name)
            if len(curr_ingr_ids) > 1:
                print('INFO: Ingredient exclusion filter "{0}" returns ambiguous results ({1})'.format(ingr_name, len(curr_ingr_ids)))
            elif len(curr_ingr_ids) == 0:
                print('INFO: Ingredient exclusion filter "{0}" returned no results, check using "list -i" command'.format(ingr_name))
            all_ingr_ids_out |= curr_ingr_ids
        filters.append(make_ingrfilter(all_ingr_ids_out, not_have=True))

    # Check for conflicts
    if len(all_ingr_ids_in & all_ingr_ids_out) > 0:
        print("WARNING: Ingredient inclusion and exclusion filters conflict, they have same ingredient listed")

    all_groups_in = set()
    if args.must_be_group is not None:
        for group_name in args.must_be_group:
            curr_groups = getGroupIds(group_name)
            all_groups_in |= curr_groups
        filters.append(make_groupfilter(all_groups_in, and_have=True))

    all_groups_out = set()
    if args.must_not_be_group is not None:
        for group_name in args.must_not_be_group:
            curr_groups = getGroupIds(group_name)
            all_groups_out |= curr_groups
        filters.append(make_groupfilter(all_groups_out, not_have=True))

    # Check for conflicts:
#    if len(all_groups_in & all_groups_out) > 0:
#        print("WARNING: Group inclusion and exclusion filters conflict, both include same group!")
        # match groups

    include_effects_in = set()
    if args.must_have_effect is not None:
        include_effects_in = set()
        for effect_name in args.must_have_effect:
            curr_effects = getEffectIds(effect_name)
            if len(curr_effects) > 1:
                print('INFO: Effect inclusion filter "{0}" returns ambiguous results {1}'.format(effect_name, len(curr_effects)))
            include_effects_in |= curr_effects
        if len(include_effects_in) > 4:
            if len(include_effects_in) <= 6:
                print('WARNING: Effect inclusion filter possibly too strict')
            else:
                print('WARNING: Effect inclusion filter too strict')
        filters.append(make_effectfilter(include_effects_in, and_have=True))

    include_effects_out = set()
    if args.must_not_have_effect is not None:
        for effect_name in args.must_not_have_effect:
            curr_effects = getEffectIds(effect_name)
            include_effects_out |= curr_effects
        filters.append(make_effectfilter(include_effects_out, not_have=True))

    if len(include_effects_in & include_effects_out) > 0:
        print("WARNING: Effect inclusion and exclusion filters conflict, they have same effect listed")

    if args.only_positives:
        # filters.append(make_groupfilter(set((1, 2, 3, 4))))
        filters.append(make_groupfilter(set((5, 6, 7, 8)), not_have=True))

    if args.only_poisons:
        # filters.append(make_groupfilter(set((5, 6, 7, 8)), not_have=True))
        filters.append(make_groupfilter(set((1, 2, 3, 4)), not_have=True))

    if args.only_positives and args.only_poisons:
        print('WARNING: both flags for only poisons and only potions used, this excludes all effect groups')

    recipes = getRecipes(allingr, filters)

    recipes.sort(key = lambda r: r.totalv, reverse=True)
    if len(recipes) > 0:
        print("Found {0} recipes".format(len(recipes)))
    else:
        print("No valid recipes found!")
    printRecipes(recipes, short=True)

def fetchPlayerStats():
    cursor.execute('''SELECT AlchemyLevel, FortifyAlchemy, AlchemicalLoreLevel, ImprovedElixirs, ImprovedPoisons, Purification FROM player WHERE ID = 1''')
    row = cursor.fetchone()
    return Player(row[0], row[1], row[2], row[3] == 1, row[4] == 1, row[5] == 1)


def set_player_levels(args):
    if args.alchemy_level is not None:
        cursor.execute('UPDATE player SET AlchemyLevel=? WHERE ID = 1', args.alchemy_level)
    if args.fortify_alchemy is not None:
        cursor.execute('UPDATE player SET FortifyAlchemy=? WHERE ID = 1', args.fortify_alchemy)
    if args.lore_level is not None:
        cursor.execute('UPDATE player SET AlchemicalLoreLevel=? WHERE ID = 1', args.lore_level)
    if args.improved_elixirs is not None:
        cursor.execute('UPDATE player SET ImprovedElixirs=? WHERE ID = 1', (int(args.improved_elixirs[0][0] in ('Y', 'y')),))
    if args.improved_poisons is not None:
        cursor.execute('UPDATE player SET ImprovedPoisons=? WHERE ID = 1', (int(args.improved_poisons[0][0] in ('Y', 'y')),))
    if args.purification is not None:
        cursor.execute('UPDATE player SET Purification=? WHERE ID = 1', (int(args.purification[0][0] in ('Y', 'y')),))


    db.commit()

    player = fetchPlayerStats()

    print("Player values are set to:")
    player.print_self()




# Parameter parser:
parser = argparse.ArgumentParser(description="Requiem Alchemy Calculator")
parser.add_argument('-o', '--output-file', nargs=1, help="Also output to file")
subparsers = parser.add_subparsers(help="use -h for with commands for available options")

parser_p = subparsers.add_parser('player', help='list ingredients, effects, effect groups')
parser_p.set_defaults(func=set_player_levels)
parser_p.add_argument('-a', '--alchemy-level', nargs=1, type=int, help="Set player Alchemy level")
parser_p.add_argument('-f', '--fortify-alchemy', nargs=1, type=int, help="Set player Alchemy fortification level")
parser_p.add_argument('-l', '--lore-level', nargs=1, type=int, choices=(0, 1, 2), help="Set player Alchemical Lore level")
parser_p.add_argument('-e', '--improved-elixirs', nargs=1, type=str, choices=("Y", "y", "N", "n"), help="Set player Improved Elixirs Perk")
parser_p.add_argument('-p', '--improved-poisons', nargs=1, type=str, choices=("Y", "y", "N", "n"), help="Set player Improved Poisons Perk")
parser_p.add_argument('-r', '--purification', nargs=1, type=str, choices=("Y", "y", "N", "n"), help="Set player Purification Perk")

parser_l = subparsers.add_parser('list', help='list ingredients, effects, effect groups')
parser_l.add_argument('-i', '--ingr-info', nargs='*', metavar="INGREDIENT", type=str, help='Display ingredient info')
parser_l.add_argument('-e', '--effect-info', nargs='*', metavar="EFFECT", type=str, help='Display effect info')
parser_l.add_argument('-g', '--group-info', nargs='*', metavar="GROUP", type=str, help='Display group info')
parser_l.set_defaults(func=list_ingredients)

parser_c = subparsers.add_parser('recipe', help='find recipes, use flags to filter, -h for list of flags')
parser_c.set_defaults(func=list_recipes)
parser_c.add_argument('-n', '--min-effects', nargs=1, metavar="N", type=int, help='Only potions with more or equal effects than minimum')
parser_c.add_argument('-m', '--max-effects', nargs=1, metavar="M", type=int, help='Only potions with less or equal effects than maximum')
parser_c.add_argument('-i', '--must-have-ingredient', nargs='+', metavar="INGREDIENT", type=str, help='Only potions which include selected ingredients')
parser_c.add_argument('-I', '--must-not-have-ingredient', nargs='+', metavar="INGREDIENT", type=str, help='Only potions which exclude selected ingredients')
parser_c.add_argument('-g', '--must-be-group', nargs='+', metavar="GROUP", type=str, help='Only potions with all effects in selected groups')
parser_c.add_argument('-G', '--must-not-be-group', nargs='+', metavar="GROUP", type=str, help='Only not potions with any effect in selected groups')
parser_c.add_argument('-e', '--must-have-effect', nargs='+', metavar="EFFECT", type=str, help='Only potions with all selected effects ')
parser_c.add_argument('-E', '--must-not-have-effect', nargs='+', metavar="EFFECT", type=str, help='Only not potions with any of selected effects')
parser_c.add_argument('-p', '--only-positives', action='store_true', default=False, help='Potions must have all positive effects')
parser_c.add_argument('-P', '--only-poisons', action='store_true', default=False, help='Potions must have all poisonous effects')

args = parser.parse_args()

# Main part
try:
    # Init global variables, open database:
    db = sqlite3.connect("ingdata.db")
    cursor = db.cursor()
    consts = Consts()
    player = Player()

    # Parse main command and execute its function
    if hasattr(args, 'func'):
        if args.output_file is not None:
            f = open(args.output_file[0], 'w')
            sys.stdout = Tee(f)
        args.func(args)
        sys.stdout = original
    else:
        parser.print_help()


finally:
    db.close()